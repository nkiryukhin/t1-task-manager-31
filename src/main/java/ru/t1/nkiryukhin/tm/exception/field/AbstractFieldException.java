package ru.t1.nkiryukhin.tm.exception.field;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.exception.AbstractException;

public abstract class AbstractFieldException extends AbstractException {

    public AbstractFieldException(@NotNull final String message) {
        super(message);
    }

}
