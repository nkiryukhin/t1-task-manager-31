package ru.t1.nkiryukhin.tm.exception.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.exception.AbstractException;

public abstract class AbstractUserException extends AbstractException {

    public AbstractUserException(@NotNull final String message) {
        super(message);
    }

}
